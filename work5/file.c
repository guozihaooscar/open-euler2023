#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>

// ext4超级块结构
struct ext4_superblock {
    uint32_t s_inodes_count;
    uint32_t s_blocks_count;
    uint32_t s_first_data_block;
};

// ext4组描述符表项结构
struct ext4_group_desc {
    uint32_t bg_block_bitmap;
    uint32_t bg_inode_table;
   
};

// ext4 inode结构
struct ext4_inode {
    uint16_t i_mode;
    uint32_t i_size;
    
};

int main() {
    // 打开设备文件（替换为你的设备文件路径）
    int fd = open("/dev/vda", O_RDONLY);
    if (fd == -1) {
        perror("Failed to open device");
        return 1;
    }

    // 读取超级块数据
    struct ext4_superblock sb;
    lseek(fd, 1024, SEEK_SET);
    read(fd, &sb, sizeof(struct ext4_superblock));

    // 读取组描述符表数据
    struct ext4_group_desc gd;
    lseek(fd, sb.s_first_data_block * 1024 + sizeof(struct ext4_superblock), SEEK_SET);
    read(fd, &gd, sizeof(struct ext4_group_desc));

    // 读取inode表项数据
    struct ext4_inode inode;
    lseek(fd, gd.bg_inode_table * 1024, SEEK_SET);
    read(fd, &inode, sizeof(struct ext4_inode));

    // 格式化打印解析的结果
    printf("ext4 Superblock details:\n");
    printf("Inodes count: %u\n", sb.s_inodes_count);
    printf("Blocks count: %u\n", sb.s_blocks_count);
    printf("First data block: %u\n", sb.s_first_data_block);


    printf("\next4 Group Descriptor details:\n");
    printf("Block bitmap block: %u\n", gd.bg_block_bitmap);
    printf("Inode table block: %u\n", gd.bg_inode_table);
   

    printf("\next4 Inode details:\n");
    printf("Mode: %o\n", inode.i_mode);
    printf("Size: %u\n", inode.i_size);
  

    // 关闭设备文件
    close(fd);

    return 0;
}