#!/bin/bash

# 确定网络接口名称，例如 eth0、wlan0 等
INTERFACE="eth0"

# 指定输出文件
OUTPUT_FILE="captured_ips.txt"

# 捕获网络包并输出到临时文件
sudo tcpdump -i $INTERFACE -c 100 -w temp_capture.pcap

# 提取 IP 地址并保存到输出文件
sudo tcpdump -r temp_capture.pcap -n | grep IP | awk '{print $3}' | cut -d'.' -f1-4 > $OUTPUT_FILE

# 清理临时文件
rm temp_capture.pcap

